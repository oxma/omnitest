import { src, dest }        from 'gulp';
import { path }             from '../gulp.path';
import env                  from 'gulp-environment'
import pug                  from 'gulp-pug';
import browserSync          from 'browser-sync';
import pugLinter            from 'gulp-pug-linter';
import w3cjs                from 'gulp-w3cjs';
import through2             from 'through2';
import plumber              from 'gulp-plumber';
import notify               from 'gulp-notify';

global.$ = {
    fs: require('fs')
};

// Сборка PUG
export const Pug = (done) => {
    src(path.pug.src)
        .pipe(env.if.development(plumber({
            errorHandler: notify.onError("Ошибка в шаблонизаторе: <%= error.message %>")
        })))
        .pipe(pug({
            pretty: '    ',
            locals : {
                authorization: JSON.parse($.fs.readFileSync('./src/__mock__/authorization/form.json', 'utf8'))
            }
        }))
        .pipe(env.if.development(dest(path.pug.build)).else(dest(path.pug.dist)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
};

// Линтер PUG (.pug-lintrc)
export const PugLinter = (done) => {
    src(path.pug.watch)
        .pipe(pugLinter({ reporter: 'default' }))
        done();
};

// W3C-валидатор
export const W3C = (done) => {
    src(path.pug.validate)
        .pipe(w3cjs())
        .pipe(through2.obj(function(file, enc, cb){
            if (!file.w3cjs.success){
                new Error('Найдена ошибка(-и) при выполнении HTML-валидации');
            }
            cb(null, file);
        }))
        done();
}

