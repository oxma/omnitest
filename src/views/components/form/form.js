/* eslint-disable no-undef */
let login       = $('.field--phone'),
    password    = $('.field--pass'),
    btn         = $('form').find('button[type="submit"]'),
    field       = $('.field'),

    state = {
        login: false,
        password: false,
        remember: false
    };

// Маска
login.mask('+7 (000) 000-00-00');

// Дизайблим в случае если js-включен
btn.prop("disabled", true);

// Функция проверки пользователя
function findUser() {
    if (users.indexOf(login.val()) !== -1 && login.val().length) {
        return true
    }
    return false
}

// Функция проверки пароля
function checkPass() {
    let pass = $('form').find('input[type="password"]').val().length;

    if( pass>=5 ) {
        return true
    }
    return false
}


// добавление активного класса 
field.on('click', function(e) {
    e.preventDefault();
    field.parent().removeClass('active')
    $(this).parent().addClass('active')
})

// Сбрасываем активные классы если кликаем вне поля
$(document).mouseup(function (e){
    e.preventDefault();
    field.parent().removeClass('active')
});

/**
* Проверка в реальном времени каждые 0.5сек после клика по полю пароль, 
* событие срабатывает если поле логин не пустое
**/

password.on('click', function(e) {
    e.preventDefault;
    if (login.val().length > 0) {
        setInterval(function(){
            state.login = findUser();
            state.password = checkPass();
        
            if ( state.password && state.login )  {
                btn.prop("disabled", false);
            } else {
                btn.prop("disabled", true);
    
                if (state.login) {
                    login.parent().addClass('success');
                    login.parent().removeClass('error');
                    // Перезапускаем интервал, чтобы в случае ошибки интервал срабатывал не по клику
                    setInterval(function(){
                        if (state.password) {
                            password.parent().addClass('success');
                            password.parent().removeClass('error');
                        } else {
                            password.parent().addClass('error');
                            password.parent().removeClass('success');
                        }
                    })
                } else {
                    login.parent().addClass('error');
                    login.parent().removeClass('success');
                    // Перезапускаем интервал, чтобы в случае ошибки интервал срабатывал не по клику
                    setInterval(function(){
                        if (state.login) {
                            login.parent().addClass('success');
                            login.parent().removeClass('error');
                        }
                    })
                }
            }
        },500);
    }
});